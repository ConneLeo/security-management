/**
 * Miniprogram Project
 * 
 * 安保管理 2.0
 * Zhe Gongshang Anbao Guanli
 * 
 * 2018.10
 * 
 */
App({
  onLaunch() {
    require('/utils/wx.js').initial();
  },
})