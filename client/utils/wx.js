/**
 * 目录
 * 
 * update() 更新
 * post() 发送 POST 请求
 * call() 调用云函数
 * modal() 
 * toast()
 * formatTime()
 * 
 */

var Config = {
  host: "https://mu-mu.cn/BWC/public/index.php/",
  _consoleRequest: true,
  _consoleResult: true,
};

var appData = {


};

function update() {
  if (wx.canIUse('getUpdateManager')) {
    var update = wx.getUpdateManager(), that = this;
    update.onCheckForUpdate(function (res) {
      console.warn(res)
      if (res.hasUpdate) {
        update.onUpdateReady(function () {
          modal('更新提示', '即将使用小程序的新版本').then(res => {
            update.applyUpdate();
          })
        })
        update.onUpdateFailed(function () {
          modal('新版本下载失败',
            '发现新版本小程序，请您在“最近使用”下拉栏中删除当前小程序，重新搜索以使用新版本。'
          ).then(res => {
            wx.navigateBack({ delta: -9 });
          })
        })
      }
    })
  };
};

// function req(url, data, log) {
//   var promise = new Promise((resolve, reject) => {
//     if (log) wx.showLoading({ title: log });
//     var that = this, logStr = url.split('/').pop() + '.php: ';
//     log = log || 'Requesting';
//     wx.request({
//       url: wx.Config.host + url + '.php',
//       data: data,
//       method: 'POST',
//       header: { 'content-type': 'application/x-www-form-urlencoded' },
//       success: function (res) {
//         wx.hideLoading();
//         res.name = logStr;  // 方便调试。
//         if (wx.Config._consoleRequest) {
//           console.group(logStr, data);
//           if (wx.Config._consoleResult) console.log(res.data)
//           console.groupEnd();
//         }
//         if (res.statusCode === 200) resolve(res);
//         else reject(res);
//       },
//       fail: function (e) {
//         wx.toast('网络连接失败')
//         reject(res);
//       }
//     })
//   });
//   return promise;
// };

/**
 * 封装post网络请求
 * @param {string}  url   目标文件
 * @param {obj}     data  携带参数
 * @param {string}  log   是否弹出加载
 */
function post(url, data, loading) {
  var promise = new Promise((resolve, reject) => {
    if (loading) wx.showLoading({ title: loading });
    data = data || {}, data.url = url;
    wx.request({
      url: wx.Config.host,
      data: data,
      method: 'POST',
      header: { 'content-type': 'application/x-www-form-urlencoded' },
      // header: { 'content-type': 'application/json' },
      success: res => {
        if (loading) wx.hideLoading();
        res.name = url;  // 方便调试。
        if (wx.Config._consoleRequest) {
          console.group(url + ':', data);
          if (wx.Config._consoleResult) console.log(res.data)
          console.groupEnd();
        }
        if (res.statusCode === 200) resolve(res);
        else reject(res);
      },
      fail: function (e) {
        wx.toast('网络连接失败，请检查网络配置或账户安全。')
        reject(res);
      }
    })
  });
  return promise;
};

/**
 * 封装云函数请求
 * @param {string}  url   目标函数
 * @param {obj}     data  携带参数
 * @param {string}  log   是否弹出加载
 */
function call(url, data, loading) {
  var promise = new Promise((resolve, reject) => {
    if (loading) wx.showLoading({ title: loading });
    data = data || {};
    wx.cloud.callFunction({
      name: url,
      data: data,
      success: res => {
        if (loading) wx.hideLoading();
        res.name = url;  // 方便调试。
        if (wx.Config._consoleRequest) {
          console.group(url + ':', data);
          if (wx.Config._consoleResult) console.log(res.result)
          console.groupEnd();
        }
        if (res.errMsg == 'cloud.callFunction:ok') resolve(res);
        else reject(res);
      },
      fail: res => {
        reject(res);
      }
    })
  });
  return promise;
};

/**
 * 封装模态提示
 * @param {str} title   标题，默认为提示
 * @param {str} content 内容
 * @param {str} cancel  取消文本，默认为取消
 * @param {str} confirm 确认文本，默认为好的
 */
function modal(title, content, cancel, confirm, cancelColor, confirmColor) {
  var promise = new Promise((resolve, reject) => {
    wx.showModal({
      title: title || '提示',
      content: content,
      showCancel: cancel || false,
      cancelText: cancel || '取消',
      confirmText: confirm || '好的',
      cancelColor: cancelColor || '',
      confirmColor: confirmColor || '',
      success: function (res) {
        if (res.confirm) resolve(res);
        else reject(res);
      },
      fail: function (res) {
        console.warn(res)
      },
    })
  });
  return promise;
};

/**
 * 封装 toast 提示
 * @param {str}  content  提示文本 
 * @param {str}  icon  提示图标，默认无
 * @param {bool} mask  是否有 mask
 */
function toast(content, icon, mask) {
  icon = icon ? 'success' : 'none';
  var promise = new Promise((resolve, reject) => {
    wx.showToast({
      title: content,
      icon: icon,
      image: '',
      duration: 2000,
      mask: mask || false,
      success: function (res) { resolve(res) },
      fail: function (res) { reject(res) },
    })
  });
  return promise;
};

/**
 * 封装 formatTime
 * @param  {int} timestamp 时间戳 
 * @param  {str} type      格式化类型
 * @return {str} formatted date & time
 */
function formatTime(date, type) {
  var year = date.getFullYear(),
    month = formatNumber(date.getMonth() + 1),
    day = formatNumber(date.getDate()),
    hours = formatNumber(date.getHours()),
    minute = formatNumber(date.getMinutes()),
    second = formatNumber(date.getSeconds());
  switch (type) {
    case 'chinese':
      return year + '年' + month + '月' + day + '日 ' +
        hours + '时' + minute + '分' + second + '秒';
    case 'date-time':
      return ([year, month, day]).join('-') + ' ' +
        ([hours, minute, second]).join(':');
    case 'date/time':
      return ([year, month, day]).join('/') + ' ' +
        ([hours, minute, second]).join(':');
    case 'time':
      return ([hours, minute, second]).join(':');
    case 'date':
      return ([year, month, day]).join('/');
  }
  return ([year, month, day]).join('-');
}
const formatNumber = n => {
  n = n.toString(); return n[1] ? n : '0' + n;
}



function initial() {
  update();
  wx.Config = Config,
    wx.post = post,
    wx.call = call,
    wx.modal = modal,
    wx.toast = toast,
    wx.appData = appData,
    wx.appData = appData,
    wx.formatTime = formatTime;
  if (!wx.cloud) wx.modal('版本过低', '请升级微信后使用。\n最低基础库版本：2.2.3')
  else wx.cloud.init({ traceUser: true })
};
module.exports = {
  initial: initial
}
